package piskiewicz.bartosz.task.githubchecker.infrastructure;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@WebAppConfiguration
public class RepoControllerIntegrationTest {

    private static final String API_URL = "/repositories/";

    @Autowired
    WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void shouldFetchRepositoryData() throws Exception {
        //given
        String owner = "bartoszpiskiewicz";
        String repository = "transactions";

        String url = String.format("%s/%s/%s", API_URL, owner, repository);

        //when
        final MvcResult mvcResult = mockMvc.perform(get(url))
                .andReturn();

        //then
        mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(jsonPath("fullName").value("bartoszpiskiewicz/transactions"))
                .andExpect(jsonPath("description").isEmpty())
                .andExpect(jsonPath("cloneUrl").value("https://github.com/bartoszpiskiewicz/transactions.git"))
                .andExpect(jsonPath("stars").value(0))
                .andExpect(jsonPath("createdAt").value("2017-04-15T12:45:47"));
    }

    @Test
    public void shouldReturnNotFoundWhenOwnerDoesNotExist() throws Exception {
        //given
        String owner = "someNotExistingUserForTestPurposes";
        String repository = "transactions";

        String url = String.format("%s/%s/%s", API_URL, owner, repository);

        //when
        mockMvc.perform(get(url))

        //then
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundWhenRepositoryDoesNotExist() throws Exception {
        //given
        String owner = "bartoszpiskiewicz";
        String repository = "someNotExistingRepository";

        String url = String.format("%s/%s/%s", API_URL, owner, repository);

        //when
        mockMvc.perform(get(url))

        //then
                .andExpect(status().isNotFound());
    }

}