package piskiewicz.bartosz.task.githubchecker.infrastructure;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import piskiewicz.bartosz.task.githubchecker.domain.fasade.RepoDto;
import piskiewicz.bartosz.task.githubchecker.domain.fasade.RepoService;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("repositories")
@AllArgsConstructor
class RepoController {

    private final RepoService repoService;

    @GetMapping("{owner}/{repository}")
    public Mono<RepoDto> getRepositoryDetails(@PathVariable String owner, @PathVariable String repository) {
        return repoService.getDetails(owner, repository);
    }

}
