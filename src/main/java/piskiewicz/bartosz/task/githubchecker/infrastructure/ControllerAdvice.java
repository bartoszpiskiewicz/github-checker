package piskiewicz.bartosz.task.githubchecker.infrastructure;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import piskiewicz.bartosz.task.githubchecker.domain.fasade.exception.RepositoryException;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(RepositoryException.class)
    public ResponseEntity<String> handleRepositoryException(RepositoryException e) {
        return new ResponseEntity<>(e.getMessage(), e.getStatus());
    }

}
