package piskiewicz.bartosz.task.githubchecker.domain;

import lombok.AllArgsConstructor;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import piskiewicz.bartosz.task.githubchecker.domain.fasade.RepoDto;
import piskiewicz.bartosz.task.githubchecker.domain.fasade.RepoService;
import piskiewicz.bartosz.task.githubchecker.domain.fasade.exception.RepositoryException;
import reactor.core.publisher.Mono;

@AllArgsConstructor
class GithubRepoService implements RepoService {

    private static final String GITHUB_API_URL = "https://api.github.com";

    private final RestTemplate restTemplate;

    @Override
    public Mono<RepoDto> getDetails(String owner, String repository) {
        String githubApiUrl = String.format("%s/repos/%s/%s", GITHUB_API_URL, owner, repository);
        try {
            final Mono<GithubDetails> githubDetails = Mono.just(restTemplate.getForEntity(githubApiUrl, GithubDetails.class)
                    .getBody());

            return githubDetails.map(this::toDto);
        } catch (HttpClientErrorException e) {
            throw new RepositoryException(e.getMessage(), e.getStatusCode());
        }
    }

    private RepoDto toDto(GithubDetails githubDetails) {
        return RepoDto.builder()
                .cloneUrl(githubDetails.getCloneUrl())
                .createdAt(githubDetails.getCreatedAt())
                .description(githubDetails.getDescription())
                .fullName(githubDetails.getFullName())
                .stars(githubDetails.getStars())
                .build();
    }
}
