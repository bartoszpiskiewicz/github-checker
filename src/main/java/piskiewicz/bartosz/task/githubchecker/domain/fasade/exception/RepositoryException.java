package piskiewicz.bartosz.task.githubchecker.domain.fasade.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class RepositoryException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public RepositoryException(HttpStatus status) {
        this.status = status;
    }

    public RepositoryException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public RepositoryException(String message, Throwable cause, HttpStatus status) {
        super(message, cause);
        this.status = status;
    }
}
