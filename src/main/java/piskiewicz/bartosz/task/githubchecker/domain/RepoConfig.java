package piskiewicz.bartosz.task.githubchecker.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import piskiewicz.bartosz.task.githubchecker.domain.fasade.RepoService;

@Configuration
class RepoConfig {

    @Bean
    RepoService repoService() {
        final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(2000);
        requestFactory.setReadTimeout(2000);

        return new GithubRepoService(new RestTemplate(requestFactory));
    }

}
