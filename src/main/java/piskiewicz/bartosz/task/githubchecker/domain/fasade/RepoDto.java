package piskiewicz.bartosz.task.githubchecker.domain.fasade;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Value
@Builder
public class RepoDto {

    String fullName;
    String description;
    String cloneUrl;
    int stars;
    LocalDateTime createdAt;

}
