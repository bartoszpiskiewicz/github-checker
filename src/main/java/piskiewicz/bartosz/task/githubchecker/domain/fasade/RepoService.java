package piskiewicz.bartosz.task.githubchecker.domain.fasade;

import reactor.core.publisher.Mono;

public interface RepoService {

    Mono<RepoDto> getDetails(String owner, String repository);
}
