package piskiewicz.bartosz.task.githubchecker.domain.fasade.exception;

import org.springframework.http.HttpStatus;

public class RepoNotFoundException extends RepositoryException {

    public RepoNotFoundException() {
        super(HttpStatus.NOT_FOUND);
    }

    public RepoNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }

    public RepoNotFoundException(String message, Throwable cause) {
        super(message, cause, HttpStatus.NOT_FOUND);
    }
}
